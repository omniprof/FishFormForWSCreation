package com.kenfogel.fishform.beans;

/**
 * Java bean for fish
 * 
 * Added final to parameters
 * 
 * @author Ken
 * @version 1.7
 *
 */
public class FishData {

	private int id;
	private String commonName;
	private String latin;
	private String ph;
	private String kh;
	private String temp;
	private String fishSize;
	private String speciesOrigin;
	private String tankSize;
	private String stocking;
	private String diet;

	/**
	 * Non-default constructor
	 * 
	 * @param id
	 * @param commonName
	 * @param latin
	 * @param ph
	 * @param kh
	 * @param temp
	 * @param fishSize
	 * @param speciesOrigin
	 * @param tankSize
	 * @param stocking
	 * @param diet
	 */
	public FishData(final int id, final String commonName, final String latin, final String ph,
			final String kh, final String temp, final String fishSize, final String speciesOrigin,
			final String tankSize, final String stocking, final String diet) {
		super();
		this.id = id;
		this.commonName = commonName;		
                this.latin = latin;
		this.ph = ph;
		this.kh = kh;
		this.temp = temp;
		this.fishSize = fishSize;
		this.speciesOrigin = speciesOrigin;
                this.tankSize = tankSize;
		this.stocking = stocking;
		this.diet = diet;
	}

	/**
	 * Default Constructor
	 */
	public FishData() {
		this(0, "", "", "", "", "", "", "", "", "", "");
	}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public String getLatin() {
        return latin;
    }

    public void setLatin(String latin) {
        this.latin = latin;
    }

    public String getPh() {
        return ph;
    }

    public void setPh(String ph) {
        this.ph = ph;
    }

    public String getKh() {
        return kh;
    }

    public void setKh(String kh) {
        this.kh = kh;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getFishSize() {
        return fishSize;
    }

    public void setFishSize(String fishSize) {
        this.fishSize = fishSize;
    }

    public String getSpeciesOrigin() {
        return speciesOrigin;
    }

    public void setSpeciesOrigin(String speciesOrigin) {
        this.speciesOrigin = speciesOrigin;
    }

    public String getTankSize() {
        return tankSize;
    }

    public void setTankSize(String tankSize) {
        this.tankSize = tankSize;
    }

    public String getStocking() {
        return stocking;
    }

    public void setStocking(String stocking) {
        this.stocking = stocking;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

}
