package com.kenfogel.fishform.persistence;

import java.sql.SQLException;

import com.kenfogel.fishform.beans.FishData;

/**
 * Interface for CRUD methods
 * 
 * @author Ken
 */
public interface FishV2DAO {

	public FishData findPrevByID(int id) throws SQLException;

	public FishData findNextByID(int id) throws SQLException;
}
