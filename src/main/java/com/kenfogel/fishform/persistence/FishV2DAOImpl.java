/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 * Added logging Changed read to 3 methods, findAll, findID and findDiet
 *
 * @author Ken Fogel
 * @version 1.7
 */
package com.kenfogel.fishform.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kenfogel.fishform.beans.FishData;

/**
 * This class implements the FishDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the user and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * @author Ken
 * @version 1.0
 *
 */
public class FishV2DAOImpl implements FishV2DAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass().getName());

    private final String url = "jdbc:mysql://localhost:3306/Aquarium";
    private final String user = "fish";
    private final String password = "kfstandard";
    
    private FishData fishData;

    public FishV2DAOImpl() {
        super();
        fishData = new FishData();
    }

    /**
     * Retrieve the next record from the given table based on the current
     * primary key using a bound bean
     *
     * @param fishData
     * @return The FishData object
     * @throws java.sql.SQLException
     */
    @Override
    public FishData findNextByID(int id) throws SQLException {
        

        String selectQuery = "SELECT ID, COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET FROM FISH WHERE ID = (SELECT MIN(ID) from FISH WHERE ID > ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, id);
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    fillFishData(resultSet, fishData);
                }
            }
        }
        log.info("Found " + id + "?: " + (fishData != null));
        return fishData;
    }

    /**
     * Retrieve the previous record from the given table based on the current
     * primary key using a bound bean
     *
     * @param id
     * @return The FishData object
     * @throws java.sql.SQLException
     */
    @Override
    public FishData findPrevByID(int id) throws SQLException {
        
        String selectQuery = "SELECT ID, COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET FROM FISH WHERE ID = (SELECT MAX(ID) from FISH WHERE ID < ?)";

        // Using try with resources, available since Java 1.7
        // Class that implement the Closable interface created in the
        // parenthesis () will be closed when the block ends.
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use PreparedStatements to guard against SQL
                // Injection
                PreparedStatement pStatement = connection.prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, id);
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    fillFishData(resultSet, fishData);
                }
            }
        }
        log.info("Found " + fishData.getId() + "?: " + (fishData != null));
        return fishData;
    }

    /**
     * Fill an existing bean that is bound to a form
     *
     * @param resultSet
     * @param fishData
     * @return
     * @throws SQLException
     */
    private void fillFishData(ResultSet resultSet, FishData fishData) throws SQLException {
        fishData.setCommonName(resultSet.getString("COMMONNAME"));
        fishData.setDiet(resultSet.getString("DIET"));
        fishData.setKh(resultSet.getString("KH"));
        fishData.setLatin(resultSet.getString("LATIN"));
        fishData.setPh(resultSet.getString("PH"));
        fishData.setFishSize(resultSet.getString("FISHSIZE"));
        fishData.setSpeciesOrigin(resultSet.getString("SPECIESORIGIN"));
        fishData.setStocking(resultSet.getString("STOCKING"));
        fishData.setTankSize(resultSet.getString("TANKSIZE"));
        fishData.setTemp(resultSet.getString("TEMP"));
        fishData.setId(resultSet.getInt("ID"));
    }
}
