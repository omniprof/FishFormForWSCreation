package com.kenfogel.fishform.controllers;

import java.sql.SQLException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.kenfogel.fishform.beans.FishData;
import com.kenfogel.fishform.persistence.FishV2DAO;

import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;

/**
 * FXML controller for FishForm
 *
 * @author Ken Fogel
 */
public class FishFormController {

    // Real programmers use logging, not System.out.println
    private final Logger log = LoggerFactory.getLogger(getClass().getName());
    
    private FishV2DAO fishDAO;
    private FishData fishData;

    @FXML
    private ResourceBundle resources;

    @FXML
    private TextField idTextField;

    @FXML
    private TextField commonNameTextField;

    @FXML
    private TextField latinTextField;

    @FXML
    private TextField phTextField;

    @FXML
    private TextField khTextField;

    @FXML
    private TextField tempTextField;

    @FXML
    private TextField fishSizeTextField;

    @FXML
    private TextField speciesOriginTextField;

    @FXML
    private TextField tankSizeTextField;

    @FXML
    private TextField stockingTextField;

    @FXML
    private TextField dietTextField;

    /**
     * Default constructor creates an instance of FishData that can be bound to
     * the form
     */
    public FishFormController() {
        super();
        fishData = new FishData();
    }

    /**
     * Transfer the data from the data bean to the fields on the form
     */
    private void fillForm() {
        idTextField.setText(fishData.getId() + "");
        commonNameTextField.setText(fishData.getCommonName());
        latinTextField.setText(fishData.getLatin());
        phTextField.setText(fishData.getPh());
        khTextField.setText(fishData.getKh());
        tempTextField.setText(fishData.getTemp());
        fishSizeTextField.setText(fishData.getFishSize());
        speciesOriginTextField.setText(fishData.getSpeciesOrigin());
        tankSizeTextField.setText(fishData.getTankSize());
        stockingTextField.setText(fishData.getStocking());
        dietTextField.setText(fishData.getDiet());
    }

    /**
     * Exit event handler
     *
     * @param event
     */
    @FXML
    void exitFish(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Advance to the next fish in the table
     *
     * @param event
     * @throws SQLException
     */
    @FXML
    void nextFish(ActionEvent event) throws SQLException {
        fishData = fishDAO.findNextByID(fishData.getId());
        fillForm();
    }

    /**
     * Move to the previous record in the table
     *
     * @param event
     * @throws SQLException
     */
    @FXML
    void prevFish(ActionEvent event) throws SQLException {
        fishData = fishDAO.findPrevByID(fishData.getId());
        fillForm();
    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database.
     *
     * @param fishDAO
     * @throws SQLException
     */
    public void setFishDAO(FishV2DAO fishDAO) throws SQLException {
        this.fishDAO = fishDAO;
        fishData = fishDAO.findNextByID(0);
        fillForm();
    }
}
